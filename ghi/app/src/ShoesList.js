import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom';


export default function ShoesList(props) {
    // const [shoes,setShoes] = useState([])
    // const fetchData = async () => {

    //     const url = 'http://localhost:8080/api/shoes/';

    //     const response = await fetch(url);

    //     if (response.ok) {
    //       const data = await response.json();
    //       setShoes(data.shoes)
    //       }

    //     }
    //     async function deleteShoe(shoe) {
    //         const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
    //         const fetchOptions = {
    //             method: "delete",
    //             headers: {
    //                 'Content-Type': 'application/json'
    //             }
    //         }
    //         await fetch(shoeUrl,fetchOptions)
    //         window.location.reload(true)

    //     useEffect(() => {
    //     fetchData();
    //     }, []);
    const deleteShoe = async (shoe) => {
        const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
        const fetchConfig = {
          method: 'delete',
          headers: {
            'Content-Type': 'application/json',
            },
        };
       const response = await fetch(shoeUrl, fetchConfig)
       if (response.ok) {
            props.getShoes()
       }
    }

  return (
    <div>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Manufacturer</th>
            <th>Shoe</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
            </tr>
        </thead>
        <tbody>
            {props.shoes.map(shoe => {
            return (
                <tr key={ shoe.id }>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.name }</td>
                <td>{ shoe.color }</td>
                <td>
                    <img src={ shoe.url } alt="" width="130px" height="110px"/>
                </td>
                <td>{ shoe.bin }</td>
                <td>
                    <button className="btn btn-outline-danger" onClick={() => deleteShoe(shoe)}>Delete</button>
                </td>
                </tr>
            );
            })}
        </tbody>
        </table>
        <nav className="navbar navbar-expand-lg navbar-light bg-info">
                <div className="container-fluid">
                    <NavLink className="navbar-brand" to="/shoes/new">Add Shoe</NavLink>
                </div>
            </nav>
    </div>
  )
}
