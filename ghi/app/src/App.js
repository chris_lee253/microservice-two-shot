import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from "react";
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import ShoesList from './ShoesList';
import CreateHatForm from './CreateHatForm';
import HatList from './HatList';

function App(props) {
  const [shoes, setShoes] = useState([])
  const [hats, setHats] = useState([]);

  const getShoes = async () => {
    const shoeUrl = "http://localhost:8080/api/shoes/"
    const response = await fetch(shoeUrl)

    if (response.ok) {
      const data = await response.json()
      const shoes = data.shoes
      setShoes(shoes)
    }
  }
  const getHats = async () => {
    const hatUrl = 'http://localhost:8090/api/hats/';
    const response = await fetch(hatUrl);

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  }
  useEffect(() => {
    getShoes();
    getHats();
  },[])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="new" element={<ShoeForm shoes={shoes} getShoes={getShoes}/>} />
            <Route path="" element={<ShoesList shoes={shoes} />}/>
          </Route>
          <Route path="hats">
            <Route path="" element={<HatList hats={hats} getHats={getHats} />} />
            <Route path="new" element={<CreateHatForm getHats={getHats} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  )
}
export default App;
