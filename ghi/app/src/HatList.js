import React from 'react';
import { NavLink } from 'react-router-dom';


function HatList(props) {
    // const [hats, setHats] = useState([]);
    // const fetchData = async () => {

    // const url = 'http://localhost:8090/api/hats/';

    // const response = await fetch(url);

    // if (response.ok) {
    //   const data = await response.json();
    //   setHats(data.hats)
    //   }
    // }
    /////////////////////////////////
    const deleteHat = async (hat) => {
        const hatUrl = `http://localhost:8090/api/hats/${hat.id}`;
        const fetchConfig = {
          method: 'delete',
          headers: {
            'Content-Type': 'application/json',
            },
        };
       const response = await fetch(hatUrl, fetchConfig)
       if (response.ok) {
            props.getHats()
       }
    }
      ///////////////////////////////////
    // useEffect(() => {
    // fetchData();
    // }, []);

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Color</th>
                        <th>Fabric</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.location }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.fabric }</td>
                            <td>
                                <img src={ hat.hat_url } alt="" width="130px" height="110px"/>
                            </td>
                            <td><button className="btn btn-outline-danger" onClick={() => deleteHat(hat)}>Delete</button></td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
            <nav className="navbar navbar-expand-lg navbar-light bg-info">
                <div className="container-fluid">
                    <NavLink className="navbar-brand" to="/hats/new">Add Hat</NavLink>
                </div>
            </nav>
        </div>

    );
  }

export default HatList;
