import React, { useState, useEffect } from 'react'

export default function ShoeForm(props) {
    const [name, setName] = useState('')
    const [manufacturer, setManufacturer] = useState('')
    const [color, setColor] = useState('')
    const [url, setUrl] = useState('')
    const [bin, setBin] = useState('')
    const [bins, setBins] = useState([])

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
      }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
      }
    const handleUrlChange = (event) => {
        const value = event.target.value;
        setUrl(value);
      }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
      }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.manufacturer = manufacturer;
        data.color = color;
        data.url = url;
        data.bin = bin;

        const shoeUrl = "http://localhost:8080/api/shoes/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setName('')
            setManufacturer('');
            setColor('');
            setUrl('');
            setBin('');
            props.getShoes();
        }
    }
        const fetchData = async () => {
            const url = 'http://localhost:8100/api/bins/';

            const response = await fetch(url);

            if (response.ok) {
              const data = await response.json();
              setBins(data.bins)


            }
          }

        useEffect(() => {
        fetchData();
        }, []);


  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={url} onChange={handleUrlChange} placeholder="Url" required type="url" name="url" id="url" className="form-control" />
                <label htmlFor="url">Picture url</label>
              </div>
              <div className="mb-3">
                <select value={bin} onChange={handleBinChange} required id="bin" name="bin" className="form-select">
                    <option value=''>Choose a bin</option>
                    {bins.map(bin => {
                        return (
                            <option key={bin.href} value={bin.href}>
                                {bin.closet_name}
                            </option>
                    );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}
