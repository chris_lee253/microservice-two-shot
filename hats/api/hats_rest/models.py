# Create your models here.

from django.db import models
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)


# Create your models here.

class Hat(models.Model):
    fabric = models.CharField(max_length=200, null=True)
    style_name = models.CharField(max_length=200)
    color=models.CharField(max_length=200, null=True)
    hat_url=models.URLField(null=True)


    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True,
        blank=True, 
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
