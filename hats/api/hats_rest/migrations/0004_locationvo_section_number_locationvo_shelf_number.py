# Generated by Django 4.0.3 on 2023-01-20 06:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_remove_locationvo_section_number_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationvo',
            name='section_number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='locationvo',
            name='shelf_number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
